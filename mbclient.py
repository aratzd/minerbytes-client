#!/usr/bin/python
"""@package docstring 
Name: mbclient
Type: Main 
Description: MinerBytes client application 
"""
import ConfigParser
import time
import logging
from clientutils import Display, Web, Image
from assetutils import Asset
from mbsocket import mbsocket
import ssl

__author__ = "Alie Abele"
__credits__ = ["Donald Howedshell", "Nathanial Kroft",]
__license__ = " "
__version__ = "1.0"
__maintainer__ = " "
__email__ = "itrss@mst.edu"
__status__ = "Development"

# Open config file ---------------------------------------------------
config = ConfigParser.ConfigParser()
config.readfp(open('client.conf'))

# Reteive configuraton details ----------------------------------------
DB_USER = config.get('database', 'USERNAME')
DB_PASS = config.get('database', 'PASSWORD')
DB_HOST = config.get('database', 'DATABASE_HOST_ADDRESS')
DB_NAME = config.get('database', 'DATABASE_NAME')
C_NAME = config.get('client', 'NAME')
RESOLUTION = config.get('client', 'RESOLUTION')
BROWSER = config.get('client', 'BROWSER')
S_PORT = config.get('database', 'SERVER_PORT')
ORIENTATION = config.get('client', 'ORIENTATION')
IMG_APP = config.get ('applications', 'IMAGE_APPLICATION')
WEB_APP = config.get ('applications', 'WEB_APPLICATION')
VID_APP = config.get ('applications', 'VIDEO_APPLICATION')
PDF_APP = config.get ('applications', 'PDF_APPLICATION')
IMG_KILL = config.get ('applications', 'IMAGE_KILL_COMMAND')
WEB_KILL = config.get ('applications', 'WEB_KILL_COMMAND')
VID_KILL = config.get ('applications', 'VIDEO_KILL_COMMAND')
PDF_KILL = config.get ('applications', 'PDF_KILL_COMMAND')
LOG = config.get ('logging', 'LOCATION')
VERBOSITY = config.get('logging', 'VERBOSITY')

# --- Set up logging ------------------------------------------------
if (VERBOSITY == 'DEBUG'):
   logging.basicConfig(filename=LOG, level=logging.DEBUG, format='%(asctime)s %(message)s')
elif (VERBOSITY == 'INFO'):
   logging.basicConfig(filename=LOG, level=logging.INFO, format='%(asctime)s %(message)s')
else:
   logging.basicConfig(filename=LOG, level=logging.WARNING, format='%(asctime)s %(message)s')

# SSL JSON socket client derived from mbsocket ----------------------------
class mbclient(mbsocket):
"""
Name: mbclient
Type: Class
Syntax: See __init__ member function
Description: Inherited from mbsocket. The client side socket for MinerBytes. 
   Incorporates SSL encription.
"""

   def __init__(self, address='localhost', port=8080):
   """
   Name: __init__
   Type: Member function
   Syntax: <some variable> = mbclient(<address>, <port>)
   Description: Constructor for mbclient class
   Passed: The target host address and port
   Returns: An instance of mbclient that will connect to address:port
   """
      super(mbclient, self).__init__(address, port)
      # Will only talk to ssl servers
      self.socket = ssl.wrap_socket(self.socket)
      self.conn = self.socket
  

   def connect(self):
   """
   Name: connect
   Type: Member function
   Syntax: <mbclient instance>.connect()
   Description: Connects the mbclient instance's socket to its end point
   Passed: Nothing
   Returns: True if the connection was sucessful, False if not.
   """
      for i in range(10):
         try:
            logging.info("Attempting to connect to %s:%s", self._address, self._port)
            self.socket.connect((self._address, self._port))
            logging.info("Connected to %s:%s", self._address, self._port)
         except Exception as msg:
            logging.error("Socket Error: %s", msg)
            time.sleep(3)
            continue
         return True
      return False


# Determine which logo page should be used
if (ORIENTATION == 'vertical'):
   logo = 'minerbytes.mst.edu/poweredby.html'
else:
   logo = 'minerbytes.mst.edu/poweredbyhoriz.html'
logging.info('Logo page set to %s', logo)

logging.info('Initializing Display object with config settings, and Blackscreen')
Show = Display(RESOLUTION, IMG_APP, WEB_APP, VID_APP, PDF_APP, IMG_KILL, WEB_KILL, VID_KILL, PDF_KILL) # the display handler
place = 0 # most recently played asset
t1 = 0 # time that the current asset started playing
BlackScreen = Web(RESOLUTION, logo, WEB_APP, WEB_KILL)

# -----------------------------------------------------------------------
# Infinite display loop
# -----------------------------------------------------------------------
current = Asset([logo, 1, 0])
while True:
   # Compose a request for the asset server
   request = [DB_USER, DB_PASS, DB_HOST, DB_NAME, C_NAME, place]
   # Connect to the MinerBytes asset server
   Connection = mbclient(DB_HOST, int(S_PORT))
   Connection.connect()
   try:
      # Attempt to send request to asset server
      Connection.sendObj(request)
      logging.debug('REQUEST SENT TO HOST %s : %s', DB_HOST, request)
   except Exception as e:
      logging.error('ERROR sending to asset server at %s : %s', DB_HOST, e)
      continue
   try: 
      # Attempt to aquire the asset server's reply
      reply = Connection.readObj()
   except Exception as e:
      logging.error('ERROR reading from asset server at %s : %s', DB_HOST, e)
      continue
   Connection.close()
   # If the current asset's display time is not up...
   if (((time.time() - t1) < current.duration) or (t1 == 0)):
      check = Asset(reply)
      # Check if the the asset ocuppying the current place has changed (eg. broadcast mode enabled)
      # If the next asset (check) is different from the current one...
      if (check.url != current.url):
         # Load the next asset
         logging.debug('Asset has changed, changing display')
         current = check
         t1 = time.time()
         Show.change(current.url, int(current.duration), RESOLUTION, str(current.place))
      time.sleep(5)
   # If the current asset's display time is up...
   else:
      logging.info('Current assets time is up, incrementing place')
      # Go on to the next place in the playlist
      place = current.place
      t1 = time.time()
      logging.info('Moving on to place %s in playlist and ressetting time', place)

