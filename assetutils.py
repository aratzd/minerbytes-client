#!/usr/bin/python
"""@package docstring
Name: assetutils
Type: Module
Description: Definition of the Asset class. To be expanded in later versions.
"""


class Asset:
""" 
Name: Asset
Type: Class
Syntax: See __init__ syntax
Description: The Asset class parses tuples into assets.   
"""
   def __init__(self, asset_t):
      """ 
      Name: __init__
      Type: Function
      Syntax: <Instance Name> = Asset(<some tuple>)
      Description: The constructor for the Asset class
      Passed: A tuple of the following format [<url>, <duration>, <place>]
      Returns: An Asset instance which includes the member variables url, duration,
         place, and mimetype.
      """
      try:
         self.duration = asset_t[1]
         self.url = asset_t[0]
         self.place = asset_t[2]
         self.mimetype = (self.url.split('.'))[-1]
      except Exception as e:
         # If the tuple passed is not of the correct format (or at least length), 
         # make a holder asset 
         print "Error: %s is not an acceptable asset." % str(asset_t)
         self.url = "researchsupport.mst.edu" # an appropriate error page should probably be 
         self.duration = 10                   # created for this
         self.place = 0
         self.mimetype = "na"

