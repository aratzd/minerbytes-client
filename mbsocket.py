"""@package docstring 
Name: mbsocket
Type: Module
Description: Contains the classes and functions to wrap a regular
   socket class and provide it with JSON string utilities and 
   handle some of the less exciting but equally important socket
   functions. 
"""
import json
import socket
import ssl
import struct
import logging
from OpenSSL import SSL

__author__ = "Alie Abele"
__credits__ = ["Donald Howedshell",]
__license__ = " "
__version__ = "1.0"
__maintainer__ = " "
__email__ = "itrss@mst.edu"
__status__ = "Development"


class mbsocket(object):
""" 
Name: mbsocket
Type: Class
Syntax: See __init__ member function
Description: Class used to create and use sockets that send and recieve
   messages formatted as JSON strings. 
"""

   def __init__(self, address='localhost', port=8000):
   """ 
   Name: __init__
   Type: Member function
   Syntax: <variable> = mbsocket(<address>, <port>)
   Description: Initializes member variables of the mbsocket class 
      including a socket instance. The socket is not connected at 
      this point. 
   Passed: The host address and the port that the socket is to 
      connect to.
   Returns: An instance of mbsocket. 
   """
      self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
      self.conn = self.socket
      self._timeout = None
      self._address = address
      self._port = port

 
   def sendObj(self, obj):
   """ 
   Name: sendObj
   Type: Member function
   Syntax: <mbsocket instance>.sendObj(<message>)
   Description: Converts the 'message' provided into a JSON string and 
      'packs' it (adds format and length information for reciever). The
      message is sent.
   Passed: The object or message to be sent
   Returns: Nothing. 'obj' is packed and sent.
   """
      msg = json.dumps(obj)
      if self.socket:
         frmt = "=%ds" % len(msg)
         packedMsg = struct.pack(frmt, msg)
         packedHdr = struct.pack('=I', len(packedMsg))
 
         self._send(packedHdr)
         self._send(packedMsg) 


   def _send(self, msg):
   """ 
   Name: _send
   Type: Member function
   Syntax: <mbsocket instance>._send(<msg>)
   Description: Sends until the amount sent is = the length of 
      message 'msg'.
   Passed: the FORMATTED message to be sent
   Returns: Nothing
   """
      sent = 0
      while sent < len(msg):
         sent += self.conn.send(msg[sent:])
 
   
   def _read(self, size):
   """ 
   Name: _read
   Type: Member function
   Syntax: <mbsocket instancce>._read(<size>)
   Description: Reads from connection self.conn until the length of 
      what is read is = size
   Passed: The size of the data to be read
   Returns: The data read from the connection
   """
      data = ''
      while len(data) < size:
         dataTmp = self.conn.recv(size-len(data))
         data += dataTmp
         if dataTmp == '':
            raise RuntimeError("socket connection broken")
      return data
 

   def _msgLength(self):
   """
   Name: _msgLength
   Type: Member function
   Syntax: <mbsocket instance>._msgLength()
   Description: Reads the size of a an incoming message. The size
      is the first 4 bytes of an incoming message.
   Passed: Nothing
   Returns: The length of the incoming message.
   """
      d = self._read(4)
      s = struct.unpack('=I', d)
      return s[0]
   

   def readObj(self):
   """
   Name: readObj
   Type: Member function
   Syntax: <mbsocket instance>.readObj()
   Description: Reads a message from the socket connection and removes 
      the formatting that was added when the sender used sendObj
   Passed: Nothing
   Returns: The message sent with the mbsocket specific formatting 
      removed
   """
      size = self._msgLength()
      data = self._read(size)
      frmt = "=%ds" % size
      msg = struct.unpack(frmt,data)
      return json.loads(msg[0])
 

   def close(self):
   """ 
   Name: close
   Type: Member function
   Syntax: <mbsocket instance>.close()
   Description: Closes the socket and the connection. 
   Passed: 
   """
      print "closing main socket"
      self._closeSocket()
      if self.socket is not self.conn:
         print "closing connection socket"
         self._closeConnection()
 

   def _closeSocket(self):
   """ 
   Name: _closeSocket
   Type: Member function
   Syntax: <mbsocket instance>._closeSocket()
   Description: calls the base class's socket close function.
   Passed: Nothing
   Returns: Nothing
   """
      self.socket.close()
 

   def _closeConnection(self):
   """
   Name: _closeConnection
   Type: Member function
   Syntax: <mbsocket instance>._closeConnection()
   Description: calls the base socket class's conn.close function
   Passed: Nothing
   Returns: Nothing
   """
      self.conn.close()
 

   def _get_timeout(self):
   """ 
   Name: _get_timeout
   Type: Member function
   Syntax: <mbsocket instance>._get_timeout()
   Description: Accessor function for member variable timeout
   Passed: Nothing
   Returns: The calling instance of mbsocket's timeout
   """
      return self._timeout
 

   def _set_timeout(self, timeout):
   """
   Name: _set_timeout 
   Type: Member function
   Syntax: <mbsocket instance>._set_timeout(<timeout>)
   Description: Sets the calling instance's base socket class's 
      timeout member variable.
   Passed: The desired timeout duration of the socket
   Returns: Nothing, just updates an inherited member variable
   """
      self._timeout = timeout
      self.socket.settimeout(timeout)
 

   def _get_address(self):
   """
   Name: _get_address
   Type: Member function
   Syntax: <mbsocket instance>._get_address()
   Description: Accessor function for the member variable _address
   Passed: Nothing
   Returns: The calling instance of mbsocket's _address
   """
      return self._address
 

   def _set_address(self, address):
   """ 
   Name: _set_address
   Type: Member function
   Syntax: <mbsocket instance>._set_address()
   Description: PLANNED
   """
      pass
 


   def _get_port(self)
   """
   Name: _get_port
   Type: Member function
   Syntax: <mbsocket instance>._get_port()
   Description: Accessor function for the _port member variable.
   Passed: Nothing
   Returns: The calling instance of mbsocket's assigned port (not
      socket base class's).
   """
      return self._port
 


   def _set_port(self, port):
   """
   Name: _set_port
   Type: Member function
   Syntax: <mbsocket instance>._set_port()
   Description: PLANNED
   """
      pass



