#!/usr/bin/python
"""@package docstring 
Name: clientutils
Type: Module
Description: Contains the classes and functions used to display 
   assets correctly. 
"""
import ConfigParser
import subprocess
import assetutils
import time
import os
import urllib

__author__ = "Alie Abele"
__credits__ = ["Donald Howedshell",]
__license__ = " "
__version__ = "1.0"
__maintainer__ = " "
__email__ = "itrss@mst.edu"
__status__ = "Development"



class Web:
""" 
Name: Web
Type: Class
Syntax: See __init__ syntax
Description: A class to handle web browser instances of subprocess 
   including opening them with the correct specifications and closing 
   them.
"""

   def __init__(self, resolution, url, web_app, web_kill):
   """  
   Name: __init__
   Type: Function
   Syntax: <instance> = Web(<resolution>, <url>, <web_app>, <web_kill>)
   Description: The Web class contructor
   Passed: The resolution the display should have, the url to which the 
      browser is to be directed, the browser application that should be
      used, and the method that should be used to close the browser.
   Returns: An instance of the Web class. Will open the specified browser
      and start it at the specified url. 
   """
      self.web_kill = web_kill.rstrip()
      self.web_app = web_app.rstrip()
      try: 
         if ((not self.web_app) or (self.web_app == "default")):       
            self.D = subprocess.Popen(["uzbl", "--geometry="+resolution, "--uri="+url, "--config=uzbl.conf"])
            self.pid = str(self.D.pid)
         else:
            command = self.web_app + " " + url
            self.D = subprocess.Popen(command.split(' '))
      except Exception as e:
         raise

  
   def changeUrl(self, url):
   """ 
   Name: changeUrl
   Type: Function
   Syntax: <instance>.changeURL(<url>)
   Description: Directs the browser used by this instance of the Web class
      to the url passed to this function. Not currently in use. Will be 
      used for planned functionality.
   Passed: The url to which the browser is to be directed
   Returns: updates the instance of Web's url member function. 
   """
      try:
         if ((not self.web_app) or (self.web_app == "default")):  
            command = 'echo \"uri '+url+'\" >> /tmp/uzbl_fifo_'+self.pid
            subprocess.call(command, shell=True)
         else:
            self.close()
            command = self.web_app + " " + url
            self.D = subprocess.Popen(command.split(' '))
      except Exception as e:
         raise


   def close(self):
   """ 
   Name: close
   Type: Function
   Syntax: <instance of Web>.close()
   Description: Closes the calling instance of the Web class's browser using 
      the close method specified by the caller's web_kill member variable.
   Passed: Nothing
   Returns: Nothing, but caller's web browser is closed.
   """
      try:
         if ((not self.web_kill) or (self.web_kill == "default")):
            self.D.terminate()
         else:
            command = self.web_kill
            kill = subprocess.Popen(command.split(' '))
      except Exception as e:
         raise

          

class Image:
""" 
Name: Image
Type: Class
Syntax: see __init__ syntax
Description: A class to handle image veiwer instances of subprocess 
   including opening them with the correct specifications and closing 
   them.
"""

   def __init__(self, url, place, img_app, img_kill):
   """ 
   Name: __init__
   Type: Member Function
   Syntax: <Image instance> = Image(<url>, <place>, <img_app>, <img_kill>)
   Description: The Image class constructor
   Passed: The url of the file that the image veiewer should open, the place
      the current asset has in its playlist, the application that should be 
      used to open the image file, and the method that should be used to 
      close the image viewing application. 
   Returns: An instance of Image. The image file specified by url will be 
      displayed using the specified image viewing application. The image 
      file will also be cached on the local host if it isn't already.
   """
      self.img_kill=img_kill
      self.place = str(place)
         # TODO: --Maybe this should be made a separate function for re-use in the 
         #       --changeImage member function in the future?
      # Local cacheing, If the local file already exists
      if (os.path.exists("/tmp/minerbytes_image"+self.place)):
         # Get size of remote file
         rLoc = urllib.urlopen(url)
         rMeta = rLoc.info()
         rSize = rMeta.getheaders("Content-Length")[0]
         # Get size of cached file 
         lMeta = os.stat("/tmp/minerbytes_image"+place)
         lSize = lMeta.st_size
         # Compare; If they're different...
         if (rSize != lSize):
            # Replace the local file with the remote one
            os.remove("/tmp/minerbytes_image"+self.place)
            urllib.urlretrieve(url, "/tmp/minerbytes_image"+self.place)
      # If the local file doesn't already exist
      else:
         # Get it
         urllib.urlretrieve(url, "/tmp/minerbytes_image"+self.place)
      # Display the file
      try:
         img_app=img_app.rstrip()
         if ((not img_app) or (img_app == "default")):
            self.I = subprocess.Popen(["feh", "-F", "-Z", "/tmp/minerbytes_image"+self.place])
         else:
            command=img_app + " " + "/tmp/minerbytes_image"+self.place
            self.I = subprocess.Popen(command.split(' '), stdin=subprocess.PIPE)
      except Exception as e:
            raise

      
   def close(self):
   """ 
   Name: close
   Type: Member Function
   Syntax: <Image instance>.close()
   Description: Closes the calling instance of the Image class's application using 
      the close method specified by the caller's img_kill member variable.
   Passed: Nothing
   Returns: Nothing, but caller's image viewing application is closed.
   """
      try:
         print "Killing image program"
         if ((not self.img_kill) or (self.img_kill == "default")):
            self.I.terminate()
         else:
            command=self.img_kill.rstrip()
            kill = subprocess.Popen(command.split(' '))
      except Exception as e:
         raise
 

   def changeImage(self, url):
   """ 
   Name: changeImage
   Type: MemberFunction
   Syntax: <instance>.changeImage(<url>)
   Description: Opens the file specified by url using the viewer employed 
      by this instance of the Image class. Not currently in use. Will be 
      used for planned functionality.
   Passed: The url of the image file to be opened.
   Returns: updates the instance of Image's url member function.
   """
      try:
         if ((not img_app) or (img_app == "default")):
            self.close(self)
            self.I = subprocess.Popen(["feh", "--fullscreen", url])
         else:
            img_app=img_app.rstrip()
            command=img_app + " " + url
            self.I = subprocess.Popen(command.split(' '),)
      except Exception as e:
         raise



class PlayVideo:
""" 
Name: PlayVideo
Type: Class
Syntax: See __init__ member function.
Description: A class to handle media player instances of subprocess 
including opening them with the correct specifications and closing 
them. For playing MP4 files.
"""

   def __init__(self, url, vid_app, vid_kill):
   """ 
   Name: __init__
   Type: Member Function
   Syntax: <PlayVideo instance> = PlayVideo(<url>, <vid_app>, <vid_kill>)
   Description: The PlayVideo class constructor
   Passed: The url of the file that the media player should open, the place
      the current asset has in its playlist, the application that should be 
      used to open the mp4 file, and the method that should be used to 
      close the media player application. 
   Returns: An instance of PlayVideo. The MP4 file specified by url will be 
      displayed using the specified media player application. 
   """
      self.vid_kill = vid_kill
      url=url.rstrip()
      try:
         if ((not vid_app) or (vid_app == "default")):
            self.V = subprocess.Popen(["omxplayer", "-b", url], stdin=subprocess.PIPE)
         else:
            vid_app=vid_app.rstrip()
            command=vid_app + " " + url
            self.V = subprocess.Popen(command.split(' '), stdin=subprocess.PIPE)
      except Exception as e:
         raise


   def close(self):
   """ 
   Name: close
   Type: Member Function
   Syntax: <PlayVideo instance>.close()
   Description: Closes the calling instance of the PlayVideo class's application using 
      the close method specified by the caller's vid_kill member variable.
   Passed: Nothing
   Returns: Nothing, but caller's media player application is closed.
   """
      try:
         if ((not self.vid_kill) or (self.vid_kill == "default")):
            self.V.communicate(input='q')
         else:
            command = self.vid_kill.rstrip()
            kill = subprocess.Popen(command.split(' '))
      except Exception as e:
         raise



class PDF:
""" 
Name: PDF
Type: Class
Syntax: See __init__ member function.
Description: A class to handle PDF viewer instances of subprocess 
including opening them with the correct specifications and closing 
them. 
"""

   def __init__(self, url, pdf_app, pdf_kill):
   """ 
   Name: __init__
   Type: Member Function
   Syntax: <PDF instance> = PDF(<url>, <pdf_app>, <pdf_kill>)
   Description: The PDF class constructor
   Passed: The url of the file that the PDF veiwer should open, the place
      the current asset has in its playlist, the application that should be 
      used to open the PDF file, and the method that should be used to 
      close the PDF viewer application. 
   Returns: An instance of PDF. The PDF file specified by url will be 
      displayed using the specified PDF viewer application. 
   """
      self.pdf_kill = pdf_kill.rstrip()
      pdf_app = pdf_app.rstrip()
      url=url.rstrip()
      try:
         if ((not pdf_app) or (pdf_app == "default")):
            self.P = subprocess.Popen(["evince", "-s",  url ], stdin=subprocess.PIPE)
         else: 
            command = pdf_app + " " + url
            self.P = subprocess.Popen(command.split(' '), stdin=subprocess.PIPE)
      except Exception as e:
         raise


   def close(self):
   """ 
   Name: close
   Type: Member Function
   Syntax: <PDF instance>.close()
   Description: Closes the calling instance of the PDF class's application using 
      the close method specified by the caller's pdf_kill member variable.
   Passed: Nothing
   Returns: Nothing, but caller's PDF viewer application is closed.
   """
      try:
         if ((not self.pdf_kill) or (self.pdf_kill == "default")):            
            self.P.terminate()
         else:
            command = self.pdf_kill
            kill = subprocess.Popen(command.split(' '))        
      except Exception as e:
     ror closing PDF app: %s" % e
    raise



class Misc:
"""
Name: Misc
Type: Class
Syntax: See __init__ member function
Description:  A class to handle miscellaneous instances of subprocess. 
   Used for unknown mimetypes. 
"""

   def __init__(self, url):
   """ 
   Name: __init__
   Type: Member Function
   Syntax: <Misc instance> = Misc(<url>)
   Description: The Misc class constructor
   Passed: The url of the file to be opened. 
   Returns: An instance of Misc. May not be able to find a compatible 
      application to open the file provided and will likely not be a 
      aesthetically acceptable result even if it does. 
   """
      self.M = subprocess.Popen(["xdg-open", url])


   def close(self):
   """ 
   Name: close
   Type: Member Function
   Syntax: <Misc instance>.close()
   Description: Attempts to close the calling instance of the Misc 
      class's application using the vanilla subprocess terminate function.
   Passed: Nothing
   Returns: Nothing, but hopefully the application instance is closed.
   """
      self.M.terminate()



class PlayYTVideo:
""" 
Name: PlayYTVideo
Type: Class
Syntax: See __init__ member function.
Description: A class to handle media player instances of subprocess 
including opening them with the correct specifications and closing 
them. For youtube MP4 files specifically.
"""

   def __init__(self, url, vid_app, vid_kill):
   """ 
   Name: __init__
   Type: Member Function
   Syntax: <PlayYTVideo instance> = PlayYTVideo(<url>, <vid_app>, <vid_kill>)
   Description: The PlayYTVideo class constructor
   Passed: The url of the youtbue video that the media player should open, the place
      the current asset has in its playlist, the application that should be 
      used to open the mp4 file, and the method that should be used to 
      close the media player application. 
   Returns: An instance of PlayYTVideo. The MP4 url of the youtube video specified 
      by the provided url will be displayed using the specified media player 
      application. 
   """
      self.vid_kill = vid_kill
      url=url.rstrip()
      try:
	 ytlink = subprocess.Popen(["youtube-dl", "-g", url], stdout=subprocess.PIPE)
	 (out, err) = ytlink.communicate()
	 ytlink = str(out)
	 ytlink = ytlink.rstrip()
         if ((not vid_app) or (vid_app == "default")):
            self.V = subprocess.Popen(["omxplayer", "-b", ytlink], stdin=subprocess.PIPE)
         else:
            vid_app=vid_app.rstrip()
            command=vid_app + " " + ytlink
            self.V = subprocess.Popen(command.split(' '), stdin=subprocess.PIPE)
      except Exception as e:
         raise


   def close(self):
   """ 
   Name: close
   Type: Member Function
   Syntax: <PlayYTVideo instance>.close()
   Description: Closes the calling instance of the PlayYTVideo class's application using 
      the close method specified by the caller's vid_kill member variable.
   Passed: Nothing
   Returns: Nothing, but caller's media player application is closed.
   """
      try:
	 if ((not self.vid_kill) or (self.vid_kill == "default")):
            self.V.communicate(input='q')
         else:
            command=self.vid_kill.rstrip()
	    print "KILL COMMAND:"+command
            kill = subprocess.Popen(command.split(' '))
      except Exception as e:
         raise



class Display:
""" 
Name: Display
Type: Class
Syntax: See __init__ member function
Description: Initializes screen display. Is passes and parses app and kill 
   configurations. Primarily handles changes from one asset to another. 
"""

   def __init__(self, res, img_app, web_app, vid_app, pdf_app, img_kill, web_kill, vid_kill, pdf_kill):
   """ 
   Name: __init__
   Type: Member function
   Syntax: <some variable> = Display(<resolution>, <img app>, <web app>, <vid app>,
      <pdf app>, <img kill>, <web kill>, <vid kill>, <pdf kill>)
   Description: Display class constructor
   Passed: The resolution the screen should be using, and the applications the
       screen should use to open and close image, html, MP4, and PDF files.
   Returned: An instance of Display. Also, the MinerBytes logo is displayed on 
       the screen.
   """
      self.D = Web(res, "https://minerbytes.mst.edu/poweredby.html", web_app, web_kill) 
      self.img_app = img_app
      self.web_app = web_app
      self.vid_app = vid_app
      self.pdf_app = pdf_app
      self.img_kill = img_kill  
      self.web_kill = web_kill   
      self.vid_kill = vid_kill
      self.pdf_kill = pdf_kill  
      

   # Handles changes from one asset to another
   def change(self, url, duration, res, place):
   """ 
   Name: change
   Type: Member function
   Syntax: <Display instance>.change(<url>, <duration>, <resolution>, <place>)
   Description: Parses asset url to determine mimetype and employs the 
      appropriate handler to display the file indicated by the url passed.
   Passed: The url to be displayed, the duration the asset is to be displayed,
      the resolution at which the asset is to be displayed, and  the asset's 
      place in its playlist. 
   Returns: Nothing but updates the member variable 'D' of the calling instance
      of Display. The file indicated by the provided url will be displayed.
   """
       try:
          if ((not url) or (not duration) or (not res) or (not place)):
             raise Exception('Could not determine URL, Duration, Res, or Place')
       
          else:
             mimetype = (url.split('.'))[-1]
             site = (url.split('.'))[1] # Only used for youtube videos
             site2 = (url.split('/'))[2] # youtu.be check
             # If the new asset is a youtube video...
             if ((site == "youtube") or (site == "googlevideo") or (site2 == "youtu.be")):
                self.D.close() 
                self.D = PlayYTVideo(url, self.vid_app, self.vid_kill) 
             # If the new asset is a MP4 file...
             elif (mimetype == "mp4"):
                self.D.close()
                self.D = PlayVideo(url, self.vid_app, self.vid_kill)
             # If the new asset is an image file...
             elif ((mimetype == "jpg") or (mimetype == "png") or (mimetype == "jpeg") or (mimetype == "bmp") or (mimetype == "tif") or (mimetype == "tiff") or (mimetype == "tga") or (mimetype == "jp2")):
                # Open the new asset over the old one
                tmp = Image(url, place, self.img_app, self.img_kill)
                # make sure it has enough time to load
                time.sleep(10)
                # Close the currently playing one
                self.D.close()
                self.D = tmp
             # If the new asset is a PDF
             elif (mimetype == "pdf"):
                self.D.close()
                self.D = PDF(url, self.pdf_app, self.pdf_kill)   
             # If the new asset is anything but the above types...
             else:
                # same as with images
                tmp = Web(res, url, self.web_app, self.web_kill)
                time.sleep(10)
                self.D.close()
                self.D = tmp
       except Exception as msg:
          raise
       
