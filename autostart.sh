#!/bin/bash
/usr/bin/unclutter -display :0 -noevents -grab &
cd ~/minerbytes-client/
until ./mbclient.py; do
	echo "Minerbytes Client crashed with exit code $?.  Respawing..." >&2
	sleep 10
done
